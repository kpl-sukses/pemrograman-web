<?php namespace App\Controllers;

class Tugas4 extends BaseController
{
	public function index()
	{
		return view('Menu');
	}
	public function tiketInput()
	{
		echo view('template/backbutton');
		echo view('tugas-04/tiket/form');
    }
	public function tiketOutput()
	{
		echo view('template/backbutton');
		echo view('tugas-04/tiket/hasil');
    }
	public function mahasiswa()
	{
		echo view('template/backbutton');
		echo view('tugas-04/database/mahasiswa');
	}
	//--------------------------------------------------------------------
}