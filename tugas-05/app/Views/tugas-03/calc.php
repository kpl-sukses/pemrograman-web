<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Calculator</title>
    <script>
        function operation(){
            let var1 = parseFloat(document.kalkulator.num1.value);
            let var2 = parseFloat(document.kalkulator.num2.value);
            let op = document.getElementById("operator").value;
            if(op=="+")
                document.kalkulator.result.value = var1+var2;
            else if(op=="-")
                document.kalkulator.result.value = var1-var2;
            else if(op=="*")
                document.kalkulator.result.value = var1*var2;
            else if(op=="/")
                document.kalkulator.result.value = var1/var2;
        }
    </script>
</head>
<body>
    <form name="kalkulator">
        First Number : <input type="number" name="num1" id="num1" placeholder="Input Number"><br><br>
        Second Number : <input type="number" name="num2" id="num2" placeholder="Input Number"><br><br>
        Result of Op. : <input type="number" name="result" id="result" placeholder="Result of Operation"><br><br>
        <select name="operatur" id="operator">
            <option value="+">Add</option>
            <option value="-">Substract</option>
            <option value="*">Multiply</option>
            <option value="/">Divide</option>
        </select>
        <input type="button" value="Operate" onclick=operation()>
    </form>
</body>
</html>