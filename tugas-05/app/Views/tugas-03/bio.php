<!DOCTYPE html>
<html>
    <head>
        <title>Guild Form Application</title>
        <script src="<?php echo base_url('js/bio.js');?>"></script>
        <style>
            body{
                text-align: center;
                padding: 0;
                background-color: rgb(36, 209, 209);
                font-family: 'Courier New', Courier, monospace;
            }
            table{
                margin: auto;
            }
            td{
                border: 2px solid
            }
            .input{
                color: #111111;
                border: 2px solid
            }
            .button {
                margin-top: 10px;
                width: 125px;
                height: 25px;
                color: rgb(36, 209, 209);
                background-color: #111111;
                border-radius: 50px;
            }
        </style>
    </head>
    <body>
        <h1>Character Sheet</h1>
        <form onsubmit="func();return false">
        <table>
            <tr>
                <td ><label for="nickname">Nickname</label></td>
                <td><input type="text" id="nickname" class="input" placeholder="Nickname"></td>
            </tr>
            <tr>
                <td><label for="npm">Faction</label></td>
                <td><select id="faction" class="input">
                    <option value="Alliance">Alliance</option>
                    <option value="Horde">Horde</option>
                </select></td>
            </tr>
            <tr>
                <td><label for="hs">Heartstone</label></td>
                <td><input type="text" id="hs" class="input" placeholder="Heartstone"></td>
            </tr>
            <tr>
                <td><label for="Job">Job</label></td>
                <td><input type="text" id="job" class="input" placeholder="Job"></td>
            </tr>
            <tr>
                <td><label for="Specialization">Specialization</label></td>
                <td><input type="text" id="specialization" class="input" placeholder="Specialization"></td>
            </tr>
        </table>
        <input type="submit" class="button" value="Submit">
        </form>
        <table id="field" style="width: 700px;" class="result"></table>
    </body>
</html>