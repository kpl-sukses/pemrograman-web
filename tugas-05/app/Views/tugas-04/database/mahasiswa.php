<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Daftar Mahasiswa KPL</title>
    <style>
        table {
            background-color: rgb(14, 200, 166);
            border: 5px solid black;
            border-collapse: collapse;
        }
        td, th {
            width: 300px;
            height: 30px;
            border: 2px solid black;
            border-collapse: collapse;
            font-size: 20px;
            font-family: 'Calibri';
        }
        .container {
            text-align: center;
        }
    </style>
</head>
<?php
  $db = \Config\Database::connect();

  $query = $db->query('SELECT * FROM mhs');
  $results = $query->getResult();
?>

<body>
    <div class="container">
        <h1>Daftar Mahasiswa KPL</h1>
        <table>
        <tr>
            <th>Nama</th>
            <th>NPM</th>
            <th>Jurusan</th>
            <th>Angkatan</th>
        </tr>
            <?php
                foreach($results as $baris){
                    echo "
                    <tr>
                        <td>$baris->nama</td>
                        <td>$baris->npm</td>
                        <td>$baris->program_studi</td>
                        <td>$baris->angkatan</td>
                    </tr>
                    ";
                }
            ?>
        </table>
    </div>
</body>
</html>