<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" integrity="sha384-JcKb8q3iqJ61gNV9KGb8thSsNjpSL0n8PARn9HuZOnIxN0hoP+VmmDGMN5t9UJ0Z" crossorigin="anonymous">
    <title>Zeppelin Ticket Order</title>
</head>
<body>
    <?php
    $nama = $_POST["nama"];
    $jk = $_POST["jk"];
    $paket = $_POST["paket"];
    $nohp = $_POST["nohp"];
    $jumlah = $_POST["jumlah"];
    $total = $jumlah * 10;
    ?>
    <div class="container" style="margin-top:50px;">
        <h1>Zeppelin Ticket Order</h1>
        <table class="table table-bordered table-dark">
            <?php
                echo "
                <tr class>
                  <td>Full Name</td>
                  <td>$nama</td>
                </tr>
                <tr>
                  <td>Gender</td>
                  <td>$jk</td>
                </tr>
                <tr>
                  <td>Package</td>
                  <td>$paket</td>
                </tr>
                <tr>
                  <td>Phone Number</td>
                  <td>$nohp</td>
                </tr>
                <tr>
                  <td>Amount of Ticket</td>
                  <td>$jumlah</td>
                </tr>
                <tr>
                  <td>Total Payment</td>
                  <td>$$total</td>
                </tr>
                ";
            ?>
        </table>
        <form action="tiketInput" style="margin-top: 50px;">
            <button type="submit" class="btn btn-primary">Buy More Ticket</button>
        </form>
    </div>
</body>
</html>