<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <style>
        body{
            text-align: center;
            background-image: url(https://images.pexels.com/photos/1591447/pexels-photo-1591447.jpeg?cs=srgb&dl=pexels-guillaume-meurice-1591447.jpg&fm=jpg);
        }
        .grid-container{
            display: grid;
            padding: 20px;
            grid-template-columns: auto auto auto auto;
            background-color: aqua;
            width: 80%;
            margin-left: 100px;
            opacity: 0.9;
        }
        .container{
            padding: 10px;
            background-color: rgb(63, 183, 58);
        }
        table{
            border: 3px solid black;
            border-collapse: collapse;
            padding: 20px;
            
        }
        td,th {
            background-color: rgb(90, 150, 90);
            border: 2px solid black;
            padding: 15px;
            margin: 10px 0;
            font-size: 20px;
        }
        a:hover{
            color: white;
            transition-duration: 0.2s;
        }
        .datadiri{
            color: white;
            font-size: 30px;;
        }
        .datadiri img{
            width: 20%;
            height: 20%;
        }
    </style>
    <title>Main Menu</title>
</head>
<body>
    <div class="datadiri">
        <img src="<?php echo base_url('images/fotoku.JPG');?>" alt="fotoku">
        <p>Mochamad Arya Bima Agfian</p>
        <p>140810190031</p>
    </div>
<div class="grid-container">
    <div class="container tugas1">
        <table>
            <tr>
                <th rowspan=2>Tugas 1</th>
                <td><a href="Tugas1/tags">Tags</a></td>
            </tr>
            <tr>
                <td><a href="Tugas1/inputs">Inputs</a></td>
            </tr>
        </table>
    </div>
    <div class="container tugas2">
    <table>
            <tr>
                <th rowspan=2>Tugas 2</th>
                <td><a href="Tugas2/mycv">My CV</a></td>
            </tr>
            <tr>
                <td><a href="Tugas2/katalog">Katalog</a></td>
            </tr>
        </table>
    </div>
    <div class="container tugas3">
    <table>
            <tr>
                <th rowspan=2>Tugas 3</th>
                <td><a href="Tugas3/bio">Bio</a></td>
            </tr>
            <tr>
                <td><a href="Tugas3/calc">Calculator</a></td>
            </tr>
        </table>
    </div>
    <div class="container tugas4">
    <table>
            <tr>
                <th rowspan=2>Tugas 4</th>
                <td><a href="Tugas4/tiketInput">Ticketing</a></td>
            </tr>
            <tr>
                <td><a href="Tugas4/mahasiswa">Mahasiswa</a></td>
            </tr>
        </table>
    </div>
</div>
</body>
</html>