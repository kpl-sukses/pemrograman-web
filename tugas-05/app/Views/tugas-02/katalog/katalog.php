<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" integrity="sha384-JcKb8q3iqJ61gNV9KGb8thSsNjpSL0n8PARn9HuZOnIxN0hoP+VmmDGMN5t9UJ0Z" crossorigin="anonymous">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url('css/katalog/style.css');?>">
    <title>Katalog Katalogan</title>
</head>
<body>
    <div class="title">
        <p>TOKO TOKOAN</p>
    </div>
    <div class="container">
        <div class="row">
            <div class="col-sm">
                <div class="image">
                    <img src="<?php echo base_url('images/1.jpg');?>" alt="1">
                </div>
                <div class="info">
                    Astolfo Dakimakura
                </div>
            </div>
            <div class="col-sm">
                <div class="image">
                    <img src="<?php echo base_url('images/2.jpg');?>" alt="2">
                </div>
                <div class="info">
                    Ayanami Figure
                </div>
            </div>
            <div class="col-sm">
                <div class="image">
                    <img src="<?php echo base_url('images/3.jpg');?>" alt="3">
                </div>
                <div class="info">
                    Chika Fujiwara Dakimakura
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-sm">
                <div class="image">
                    <img src="<?php echo base_url('images/4.jpg');?>" alt="4">
                </div>
                <div class="info">
                    Kaffu Chino Plushie
                </div>
            </div>
            <div class="col-sm">
                <div class="image">
                    <img src="<?php echo base_url('images/5.jpg');?>" alt="5">
                </div>
                <div class="info">
                    Herrscher of the Void X Ice Queen
                </div>
            </div>
            <div class="col-sm">
                <div class="image">
                    <img src="<?php echo base_url('images/6.jpg');?>" alt="6">
                </div>
                <div class="info">
                    Kizuna Ai Figure
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-sm">
                <div class="image">
                    <img src="<?php echo base_url('images/7.jpg');?>" alt="7">
                </div>
                <div class="info">
                    Hatsune Miku Plushie
                </div>
            </div>
            <div class="col-sm">
                <div class="image">
                    <img src="<?php echo base_url('images/8.jpg');?>" alt="8">
                </div>
                <div class="info">
                    Miku Nakano Plushie
                </div>
            </div>
            <div class="col-sm">
                <div class="image">
                    <img src="<?php echo base_url('images/9.jpg');?>" alt="9">
                </div>
                <div class="info">
                    Yui Yuigahama Figure
                </div>
            </div>
        </div>
    </div>
</body>
</html>