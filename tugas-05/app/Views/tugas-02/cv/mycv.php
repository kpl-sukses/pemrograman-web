<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>My CV</title>
    <script src="https://kit.fontawesome.com/b99e675b6e.js"></script>
    <link rel="stylesheet" href="<?php echo base_url('css/cv/mycv.css');?>">
</head>
<body>
    <div class="resume">
        <div class="resume-kiri">
            <div class="resume-profile">
                <img src="<?php echo base_url('images/fotoku.JPG');?>" alt="Fotoku">
            </div>
            <div class="resume-content">
                <div class="resume-item resume-info">
                    <div class="title">
                        <p class="bold">mochamad arya bima agfian</p>
                        <p class="regular">Student</p>
                    </div>
                    <ul>
                        <li>
                            <div class="icon">
                                <i class="fas fa-map-signs" aria-hidden="true"></i>
                            </div>
                            <div class="data">
                                Sukaasih Raya No.58B <br> Bandung
                            </div>
                        </li>
                        <li>
                            <div class="icon">
                                <i class="fas fa-mobile-alt" aria-hidden="true"></i>
                            </div>
                            <div class="data">
                                +62 821 2700 6961
                            </div>
                        </li>
                        <li>
                            <div class="icon">
                                <i class="fas fa-envelope" aria-hidden="true"></i>
                            </div>
                            <div class="data">
                                aaaabim@gmail.com
                            </div>
                        </li>
                        <li>
                            <div class="icon">
                                <i class="fab fa-github" aria-hidden="true"></i>
                            </div>
                            <div class="data">
                                github.com/aaaabima
                            </div>
                        </li>
                    </ul>
                </div>
                <div class="resume-item resume-skills">
                    <div class="title">
                        <p class="bold">skills</p>
                    </div>
                    <ul>
                        <li>
                            <div class="skill-name">
                                C++
                            </div>
                            <div class="skill-progress">
                                <span style="width: 60%;"></span>
                            </div>
                            <div class="skill-percentage">60%</div>
                        </li>
                        <li>
                            <div class="skill-name">
                                HTML
                            </div>
                            <div class="skill-progress">
                                <span style="width: 30%;"></span>
                            </div>
                            <div class="skill-percentage">30%</div>
                        </li>
                        <li>
                            <div class="skill-name">
                                CSS
                            </div>
                            <div class="skill-progress">
                                <span style="width: 25%;"></span>
                            </div>
                            <div class="skill-percentage">25%</div>
                        </li>
                        <li>
                            <div class="skill-name">
                                Java
                            </div>
                            <div class="skill-progress">
                                <span style="width: 20%;"></span>
                            </div>
                            <div class="skill-percentage">20%</div>
                        </li>
                        <li>
                            <div class="skill-name">
                                MySQL Query
                            </div>
                            <div class="skill-progress">
                                <span style="width: 45%;"></span>
                            </div>
                            <div class="skill-percentage">45%</div>
                        </li>
                    </ul>
                </div>
                <div class="resume-item resume-socials">
                    <div class="title">
                        <p class="bold">Socials</p>
                    </div>
                    <ul>
                        <li>
                            <div class="icon">
                                <i class="fab fa-line" aria-hidden="true"></i>
                            </div>
                            <div class="data">
                                <p class="semi-bold">
                                    Line
                                </p>
                                <p>aaaabima</p>
                            </div>
                        </li>
                        <li>
                            <div class="icon">
                                <i class="fab fa-instagram-square" aria-hidden="true"></i>
                            </div>
                            <div class="data">
                                <p class="semi-bold">
                                    Instagram
                                </p>
                                <p>aaaabima</p>
                            </div>
                        </li>
                        <li>
                            <div class="icon">
                                <i class="fab fa-discord" aria-hidden="true"></i>
                            </div>
                            <div class="data">
                                <p class="semi-bold">
                                    Discord
                                </p>
                                <p>Domineundo#9012</p>
                            </div>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="resume-kanan">
            <div class="resume-item resume-about">
                <div class="title">
                    <p class="bold">About Me</p>
                </div>
                <p>I like to interact with people and to discuss any kind of topic. Game and music is my domain. Also interested in foreign language such as English or Japanese. Always ready to help whenever you ask.</p>
            </div>
            <div class="resume-item resume-organization">
                <div class="title">
                    <p class="bold">Organization</p>
                </div>
                <ul>
                    <li>
                        <div class="date">2017</div>
                        <div class="info">
                            <p class="semi-bold">Staff of Equipment Deparment MB Gita Pakuan</p>
                        </div>
                    </li>
                    <li>
                        <div class="date">2018 - 2019</div>
                        <div class="info">
                            <p class="semi-bold">Head of Equipment Deparment MB Gita Pakuan</p>
                        </div>
                    </li>
                    <li>
                        <div class="date">2019 - Present</div>
                        <div class="info">
                            <p class="semi-bold">Staff of Administration and Secretary Bureau Himatif FMIPA Unpad</p>
                        </div>
                    </li>
                </ul>
            </div>
            <div class="resume-item resume-education">
                <div class="title">
                    <p class="bold">Education</p>
                </div>
                <ul>
                    <li>
                        <div class="date">2016 - 2019</div>
                        <div class="info">
                            <p class="semi-bold">SMAN 24 Bandung</p>
                        </div>
                    </li>
                    <li>
                        <div class="date">2019 - Present</div>
                        <div class="info">
                            <p class="semi-bold">Universitas Padjadjaran</p>
                        </div>
                    </li>
                </ul>
            </div>
            <div class="resume-item resume-interests">
                <div class="title">
                    <p class="bold">Interests</p>
                </div>
                <ul>
                    <li><i class="fas fa-users" aria-hidden="true"></i></li>
                    <li><i class="fas fa-camera" aria-hidden="true"></i></li>
                    <li><i class="fas fa-gamepad" aria-hidden="true"></i></li>
                    <li><i class="fas fa-music" aria-hidden="true"></i></li>
                </ul>
            </div>
        </div>
    </div>
</body>
</html>