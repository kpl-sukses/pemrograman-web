<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <style>
        .return-button{
            position:fixed;
            bottom: 95%;
            right: 5%;
            font-size: 20px;
        }
        .return-button button{
            width: 120px;
            height: 40px;
            border: 4px solid black;
            border-radius: 10px;
        }
        .return-button button:hover{
            background-color: black;
            color: white;
            transition-duration: 0.3s;
        }
    </style>
</head>
<body>
    <div class="return-button">
        <a href="<?php echo base_url();?>"><button>Kembali</button></a>
    </div>
