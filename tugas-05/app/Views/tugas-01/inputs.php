<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Registration</title>
</head>
<body style="background-color: aquamarine;">
    <div style="text-align: center;">
        <h1>Guild Registration Form</h1>
        <form name="register">
            <label>Character Name : </label><input type="text" name="nama" size="30"><br><br>
            <label for="motive">Joining Motivation : </label>
            <textarea name="motive" rows=20 cols=30></textarea><br><br>
            <span>Proffesions :</span><br>
                <input type="checkbox" name="alchemy" value="Alchemy">
                <label for="alchemy">Alchemy</label>
                <input type="checkbox" name="enchanting" value="Enchanting">
                <label for="enchanting">Enchanting</label>
                <input type="checkbox" name="inscription" value="Inscription">
                <label for="inscription">Inscription</label>
                <input type="checkbox" name="jewelcrafting" value="Jewelcrafting">
                <label for="jewelcrafting">Jewelcrafting</label>
                <input type="checkbox" name="tailoring" value="Tailoring">
                <label for="tailoring">Tailoring</label><br><br>
            <label for="roles">Choose your role:</label>
                <select name="roles">
                  <option value="questing">Questing</option>
                  <option value="achiever">Achiever</option>
                  <option value="raider">Raider</option>
                  <option value="gladiator">Gladiator</option>
                </select><br><br>
            <input type="submit" value="Submit">
        </form>
    </div>
</body>
</html>