<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Tags in HTML</title>
</head>
<body style="background-color: aquamarine;">
    <h1>Usable Tags in HTML</h1>
    <h1>h1 to h6 are used to define a heading on HTML. Smaller number, greater text size.</h1>
    <h1>This is h1</h1>
    <h2>This is h2</h2>
    <h3>This is h3</h3>
    <h4>This is h4</h4>
    <h5>This is h5</h5>
    <h6>This is h6</h6>
    <p>P tag is used to create a block-line paragraph.</p>
    <p>>>>This sentence is written under P tag.<<<</p>
    <span>Span is used to contain an in-line text.</span><br>
    <span style="background-color: white;">Span will also use block as long as the next needed.</span><br>
    <div style="background-color: wheat;">This is Div. Which used entire line to contain text.</div>
    <p>Next one is Bold. You know what bold is, it makes text's color thicker <b>Like This</b></p>
    <p>This one is Strong, makes your text looks important (and also bold). <strong>Look, i'm strong.</strong></p>
    <p>There's also Italic. Now your text is the Leaning Tower of Pisa. <i>Your text needs Mizone.</i></p>
    <p>Similar to italic, we have Emphasize. Use it to emphasize text, of course. <em>This looks important because it's leaning.</em></p>
    <p>Here is Mark, not as a person but as a tag. It will highlight your text. <mark>This text is marked for something i don't know.</mark></p>
    <p>And now we have Del and Ins. Use del to write a text with strike through line. Use ins to write a text with underline. </p>
    <p>You can use these tags to <del>meme</del> <ins>correct</ins> text in HTML. Though you'll not use it if you don't make any mistake.</p>
</body>
</html>