<?php namespace Config;

// Create a new instance of our RouteCollection class.
$routes = Services::routes();

// Load the system's routing file first, so that the app and ENVIRONMENT
// can override as needed.
if (file_exists(SYSTEMPATH . 'Config/Routes.php'))
{
	require SYSTEMPATH . 'Config/Routes.php';
}

/**
 * --------------------------------------------------------------------
 * Router Setup
 * --------------------------------------------------------------------
 */
$routes->setDefaultNamespace('App\Controllers');
$routes->setDefaultController('Home');
$routes->setDefaultMethod('index');
$routes->setTranslateURIDashes(false);
$routes->set404Override();
$routes->setAutoRoute(true);

/**
 * --------------------------------------------------------------------
 * Route Definitions
 * --------------------------------------------------------------------
 */

// We get a performance increase by specifying the default
// route since we don't have to scan directories.
$routes->get('/', 'Home::index');
$routes->get('/Tugas1', 'Tugas1::index');
$routes->get('/Tugas1/tags', 'Tugas1::tags');
$routes->get('/Tugas1/inputs', 'Tugas1::inputs');
$routes->get('/Tugas2', 'Tugas2::index');
$routes->get('/Tugas2/mycv', 'Tugas2::mycv');
$routes->get('/Tugas2/katalog', 'Tugas2::katalog');
$routes->get('/Tugas3', 'Tugas3::index');
$routes->get('/Tugas3/bio', 'Tugas3::bio');
$routes->get('/Tugas3/calc', 'Tugas3::calc');
$routes->get('/Tugas4', 'Tugas4::index');
$routes->get('/Tugas4/form', 'Tugas4::tiketInput');
$routes->get('/Tugas4/hasil', 'Tugas4::tiketOutput');
$routes->get('/Tugas4/mahasiswa', 'Tugas4::mahasiswa');

/**
 * --------------------------------------------------------------------
 * Additional Routing
 * --------------------------------------------------------------------
 *
 * There will often be times that you need additional routing and you
 * need it to be able to override any defaults in this file. Environment
 * based routes is one such time. require() additional route files here
 * to make that happen.
 *
 * You will have access to the $routes object within that file without
 * needing to reload it.
 */
if (file_exists(APPPATH . 'Config/' . ENVIRONMENT . '/Routes.php'))
{
	require APPPATH . 'Config/' . ENVIRONMENT . '/Routes.php';
}
