<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Daftar Mahasiswa KPL</title>
    <style>
        table {
            background-color: rgb(14, 200, 166);
            border: 5px solid black;
            border-collapse: collapse;
        }
        td, th {
            width: 300px;
            height: 30px;
            border: 2px solid black;
            border-collapse: collapse;
            font-size: 20px;
            font-family: 'Calibri';
        }
        .container {
            text-align: center;
        }
    </style>
</head>
<?php
  $host= "localhost";
  $database = "mahasiswa";
  $user = "root";
  $password = "Sukardiingat315";
  $dsn = "mysql:host=$host;dbname=$database";

  $conn = new PDO ($dsn, $user, $password);

  $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
  $stm = $conn->query("SELECT * FROM mhs");
  $rows = $stm->fetchAll(PDO::FETCH_ASSOC);
?>

<body>
    <div class="container">
        <h1>Daftar Mahasiswa KPL</h1>
        <table style="">
        <tr>
            <th>Nama</th>
            <th>NPM</th>
            <th>Jurusan</th>
            <th>Angkatan</th>
        </tr>
            <?php
                foreach($rows as $baris){
                    echo "
                    <tr>
                        <td>{$baris['npm']}</td>
                        <td>{$baris['nama']}</td>
                        <td>{$baris['jurusan']}</td>
                        <td>{$baris['angkatan']}</td>
                    </tr>
                    ";
                }
            ?>
        </table>
    </div>
</body>
</html>